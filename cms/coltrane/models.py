from django.db import models
from django.contrib import admin
from django.contrib.auth.models  import User
from django.conf import settings


from tagging.fields import TagField
from markdown import markdown

import datetime

class Category(models.Model):
	title=models.CharField(max_length=50,help_text="the tile for the blog")
	#slug=models.SlugField(prepopulate_from=['title'],unique=True)
	slug=models.SlugField(unique=True,help_text="the slug should be unique")
	description=models.TextField()

	class Admin:
		pass
	class Meta:
		ordering=['title']
		verbose_name_plural="Categories"

	def __unicode__(self):
		return self.title
	def get_absolute_url(self):
		return "/coltrane/category/%s/" %self.slug
	

class Entry(models.Model):
	title =models.CharField(max_length=200, help_text="the title for this entry")
	excerpt=models.CharField(blank=True,max_length=100)
	body=models.TextField()
	pub_date=models.DateTimeField(default=datetime.datetime.now)
	slug=models.SlugField(unique=True,help_text="the slug should be unique",unique_for_date='pub_date')
	author=models.ForeignKey(User)
	enable_comment=models.BooleanField(default=True)
	featured=models.BooleanField(default=False)

	LIVE_STATUS=1
	DRAFT_STATUS=2

	STATUS_CHOICES=(
			(LIVE_STATUS,'Live'),
			(DRAFT_STATUS,'Draft')
		       )

	status=models.IntegerField(choices=STATUS_CHOICES,default=LIVE_STATUS)

	categories=models.ManyToManyField(Category)

	tags=TagField()

	body_html=models.TextField(blank=True, editable=False)
	excerpt_html=models.TextField(blank=True, editable=False)


	def save(self):
		self.body_html=markdown(self.body)
		if(self.excerpt):
			self.excerpt_html=markdown(self.excerpt)

		super(Entry,self).save()

	
	class Meta:
		verbose_name_plural="Entries"
		ordering=['-pub_date']
	class Admin:
		pass

	def __unicode__(self):
		return self.title
	
	#def get_absolute_url(self):
	#	return "/coltrane/%s/%s/%s/" %(self.pub_date.strftime('%Y').lower(),self.pub_date.strftime('%b').lower(), self.pub_date.strftime('%d').lower())
	#	return "/coltrane/%s/%s/" %(self.pub_date.strftime("%Y/%b/%d").lower(),self.slug)
	@models.permalink
	def get_absolute_url(self):

		return('coltrane_entry_detail', (),
			{'year': self.pub_date.strftime('%Y'),
			 'month': self.pub_date.strftime('%b').lower(),
			 'day': self.pub_date.strftime('%d'),
			 'slug': self.slug})


class Link(models.Model):

	url = models.URLField(unique=True)
	title = models.CharField(max_length=250)
	slug = models.SlugField(unique_for_date='pub_date', help_text='Must be unique for the publication date.')
	description = models.TextField(blank=True)
	description_html = models.TextField(editable=False, blank=True)
	via_name = models.CharField(
		'Via',
		max_length=250,
		blank=True,
		help_text='The name of the person whose site you spotted the link on. Optional.')
	via_url = models.URLField(
		'Via URL',
		blank=True,
		help_text='The URL of the site where you spotted the link. Optional.')
	tags = TagField()

	
	# metadata
	enable_comments = models.BooleanField(default=True)
	post_elsewhere = models.BooleanField(
		'Post to Delicious',
		default=True,
		help_text='If checked, this link will also be posted to your del.icio.us account.')
	posted_by = models.ForeignKey(User)
	pub_date = models.DateTimeField(default=datetime.datetime.now)


	class Meta:
		ordering = ['-pub_date']

	class Admin:
		pass

	def __unicode__(self):
		return self.title

	def save(self):

		if self.description:
			self.description_html = markdown(self.description)

		if not self.id and self.post_elsewhere:
			pass

			# del.icio.us authentication doesn't work right now
			# I started playing with twitter
			# but it's a real pain in the ass for a simple tutorial

			import pydelicious
			from django.utils.encoding import smart_str

			pydelicious.add(
				settings.DELICIOUS_USER,
				settings.DELICIOUS_PASSWORD,
				smart_str(self.url),
				smart_str(self.title),
				smart_str(self.tags))

		super(Link, self).save()

	@models.permalink
	def get_absolute_url(self):

		return('coltrane_link_detail', (),
			{'year': self.pub_date.strftime('%Y'),
			 'month': self.pub_date.strftime('%b').lower(),
			 'day': self.pub_date.strftime('%d'),
			 'slug': self.slug})

admin.site.register(Category)
admin.site.register(Entry)
admin.site.register(Link)
