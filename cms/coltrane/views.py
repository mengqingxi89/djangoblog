from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from coltrane.models import Entry,Category

def entry_index(request):

	entries=Entry.objects.all()
	#entries=Entry.objects.get(title='a')
	#entries=get_object_or_404(Entry,title='a')
	return render_to_response('coltrane/entry_index.html',
			{'entry_list':entries} )



def entry_detail(request, year, month, day, slug):
	import datetime, time
	date_stamp = time.strptime(year+month+day, "%Y%b%d")
	pub_date = datetime.date(*date_stamp[:3])
	return render_to_response('coltrane/entry_detail.html',
	{ 'entry': Entry.objects.get(pub_date__year=
	pub_date.year,
	pub_date__month=pub_date.month,
	pub_date__day=pub_date.day,
	slug=slug) })
	

def category_index(request):
	categories=Category.objects.all()
	return render_to_response('coltrane/category_index.html',{'category_list':categories} )



def category_detail(request, slug):
	#print 'here?'
	category=get_object_or_404(Category,slug=slug)
	return render_to_response('coltrane/category_detail.html',{'category_list':category.entry_set.all(), 'category':category})

