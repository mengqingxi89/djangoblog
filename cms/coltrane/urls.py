#from django.conf.urls.defaults import patterns, include, url
from django.conf.urls.defaults import *
from coltrane.models import Entry, Category, Link
from tagging.models import Tag

entry_info_dict={
	'queryset':Entry.objects.all(),
	'date_field':'pub_date',
}

link_info_dict={
	'queryset':Link.objects.all(),
	'date_field':'pub_date',
}
urlpatterns = patterns('django.views.generic.date_based',

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

     url(r'^entry/$', 'archive_index',entry_info_dict,'coltrane_entry_archive_index'),
     url(r'^entry/(?P<year>\d{4})/$', 'archive_year',entry_info_dict,'coltrane_entry_archive_year'),
     url(r'^entry/(?P<year>\d{4})/(?P<month>\w{3})/$', 'archive_month',entry_info_dict,'coltrane_entry_archive_month'),
     url(r'^entry/(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{2})/$', 'archive_day',entry_info_dict,'coltrane_entry_archive_day'),
     url(r'^entry/(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$', 'object_detail',entry_info_dict,'coltrane_entry_detail'),
#	(r'^weblog/(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{2})/(P?<slug>[-\w]+)/$','coltrane.views.entry_detail'),


)
urlpatterns += patterns('django.views.generic.date_based',

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

     url(r'^link/$', 'archive_index',link_info_dict,'coltrane_link_archive_index'),
     url(r'^link/(?P<year>\d{4})/$', 'archive_year',link_info_dict,'coltrane_link_archive_year'),
     url(r'^link/(?P<year>\d{4})/(?P<month>\w{3})/$', 'archive_month',link_info_dict,'coltrane_link_archive_month'),
     url(r'^link/(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{2})/$', 'archive_day',link_info_dict,'coltrane_link_archive_day'),
     )
urlpatterns += patterns('',
     #url(r'^category/$', 'coltrane.views.category_index'),
     #url(r'^category/(?P<slug>[-\w]+)/$', 'coltrane.views.category_detail'),
     url(r'^category/$', 'django.views.generic.list_detail.object_list',{'queryset':Category.objects.all()}),
     url(r'^category/(?P<slug>[-\w]+)/$', 'django.views.generic.list_detail.object_detail'),
     url(r'^tag/', 'django.views.generic.list_detail.object_list',{'queryset':Tag.objects.all()}),
     url(r'^tag/entries/(?P<tag>[-\w]+)/$', 'tagging.views.tagged_object_list',{'queryset':Tag.objects.all()}),
     )
