from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin 
     
class UserProfile(models.Model):
	    url = models.URLField()
	    home_address = models.TextField()
	    phone_numer = models.CharField(max_length=15)
	    user = models.ForeignKey(User, unique=True)
admin.site.register(UserProfile)
